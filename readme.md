# Exact automations

In this repo we're keeping our Exact automations, since Exact doesn't exactly (hehe) have all the features we want,
we're building the automations using [PhantomJS](http://phantomjs.org/) (You need to download this and put it in the project root folder to make this project work) which is a headless browser which is commanded by JavaScript.
The cronjobs that execute the executables automatically are ran on Linux. And the status updates are send by Python.


### Bank processing
Found in ```bank-processing.js``` -- this script automatically logs onto Exact and automatically presses the bank import process button.
**You have to create an** ```account.json``` **file in the project root folder. The file looks like:**
```
{
    "username": "John",
    "password": "Doe"
}
```
You can execute the script in your terminal by writing the following command: 
```
./phantomjs bank-processing.js
```
