// <SETUP
var page = require("webpage").create();
var fs = require('fs');
var fullDate = new Date();
var startURL = "https://start.exactonline.nl/docs/Login.aspx";
var messages = {
    start: "STARTING AUTOMATIC BANK PROCESSING",
    creatingLogFile: "Thus creating said file",
    logStartNotifyText: "Starting log",
    pageTryingToLoadURLNotFound: " doesn't exist in script -- so Exact changed their flow, or encountered an unforseen error.",
    URLMissmatch: "URL doesn't match anything in script: ",
    loginError: "Encountered an error during login",
    importerError: "Importer encountered an error: ",
    creatingScreenshot: "Creating screenshot at: ",
    startingLoginFlow: "Start login flow:",
    loggingInToDashboard: "Logging in into dashboard with username: ",
    goingToLoginWithUsername: "Going to login with the following username: ",
    loginBeingHandledByExact: "Login is being handled by Exact",
    loginSucces: "Succesfully logged in",
    clickingImportButton: "Clicking on import button",
    clickedImportButton: "Clicked on import button",
    doneFlow: "Done with flow",
    logEndMessage: "Ending log.\n",
    phantomEndMessage: "PhantomJS is going to quit now.\n",
    URLChanged: "URL changed to: ",
    URLExpected: " is expected and defined by and in script.",
    scriptExecuteFlow: "Script is going to execute the following flow: ",
    openRequest: "Opening request to: ",
    emailRequestSuccesful: "Request to email script was succesful.",
    emailRequestFailed: "Error: request to email script failed",
    movingFromIframeWorkflow: "Moving away from iframe workflow, by opening the views without the menu bar",
    atDashboard: "Script is at dashboard now, without menubar",
    importSucces: "Import was succesful"
}

page.settings.userAgent = 'SpecialAgent';
// SETUP>

// <LOGGING
var logPath;
var logToConsole = function (message, colour, opacity) {
    if (!colour) colour = 37;
    if (!opacity) opacity = 1;
    var printMessage = "\033[" + opacity + ";" + colour + "m" + message + "\033[0m";
    console.log(printMessage);
}
var log = function (message, colour, opacity) {
    if (!colour) colour = 37;
    if (!opacity) opacity = 1;
    logToConsole(message, colour, opacity);

    message = logDate() + " " + message;
    var logFileName = logPath + "process.log";
    var content = "";

    try {
        content = fs.read(logFileName);
    } catch (e) {
        logToConsole(e, 33, 1);
        logToConsole(messages.creatingLogFile, 32, 1);
    }

    fs.write(logFileName, content + "\n" + message, 'w');

}
var processDate = function (fetch) {
    var hour = fullDate.getHours();
    var minutes = fullDate.getMinutes();
    var year = fullDate.getFullYear();
    var month = fullDate.getMonth() + 1; // beware: January = 0; February = 1, etc.
    var day = fullDate.getDate();

    if (fetch === true) return year + "-" + month + "-" + day + "-" + hour + "-" + minutes;
    if (fetch !== true) processedDate = year + "-" + month + "-" + day + "-" + hour + "h" + minutes + "m";
}
var logDate = function () {
    var seconds = fullDate.getSeconds();
    var miliseconds = fullDate.getMilliseconds();
    var logDate = processDate(true) + ":" + seconds + ":" + miliseconds + " --";
    return logDate;
}
var createLogFolder = function () {
    logPath = "logs/" + processedDate + "/";
    if (fs.makeDirectory(logPath)) {
        // file doesn't already exist.
        logToConsole('"' + logPath + '" was created.', 37, 2);
    } else {
        logToConsole('"' + logPath + '" already exists.', 37, 2);
    }
}
var getScreenshotURL = function (name) {
    return logPath + name;
}
var logScreenshot = function (name) {
    var fullName = getScreenshotURL(name);
    log(messages.creatingScreenshot + fullName, 34);
    page.render(fullName);
}
var logInit = function () {
    processDate();
    createLogFolder();
    log(messages.logStartNotifyText, 37);
}
// LOGGING>

// <RETRIEVE ACCOUNT INFO
var accountInfo = {};
var retrieveAccountInfo = function () {
    var rawAccountInfo = fs.read("account.json");
    accountInfo = JSON.parse(rawAccountInfo);
    log(messages.goingToLoginWithUsername + accountInfo.username, 37, 1);
}
// RETRIEVE ACCOUNT INFO>

// <LOGIN HANDLER
var login = {
    visited: false
};
login.init = function () {
    if (login.visited === false) {
        log(messages.startingLoginFlow);
        log(messages.loggingInToDashboard + accountInfo.username);
        login.fillAndSubmitForm();
        login.visited = true;
    } else {
        // there should be logic to check if the user is logged in here.
        log(messages.loginBeingHandledByExact);
        login.checkIfLoggedIn();
    }
}
login.checkIfLoggedIn = function () {
    var loggedIn = evaluate(page, function () {
        try {
            var error = document.getElementById("LoginForm_ErrorList").innerText;
            return error;
        } catch (e) {
            return true;
        }
    });

    if (loggedIn !== true) {
        error.loginFailed(loggedIn());
    } else {
        log(messages.loginSucces, 32);
    }
}
login.fillAndSubmitForm = function () {
    evaluate(page, function (accountInfo) {
        document.getElementById("LoginForm_UserName").value = accountInfo.username;
        document.getElementById("LoginForm_Password").value = accountInfo.password;
        document.getElementById("LoginForm_btnSave").click();
        return document.getElementById('login').innerHTML;
    }, accountInfo);
}
// LOGIN HANDLER>

// <REDIRECT SYSTEM
var moveAwayFromIframeSystem = function () {
    log(messages.movingFromIframeWorkflow);
    page.open("https://start.exactonline.nl/financial/dashboard");
}
var moveToImporterFromDashboard = function () {
    setTimeout(function () {
        log(messages.atDashboard);
        logScreenshot("dashboard.png");

        var dashboardOverviewEvaluate = evaluate(page, function () {
            document.getElementById("btnCashflowImportStatement").click();
            return document.getElementById("btnCashflowImportStatement").innerHTML;
        });

        console.log("Browser is at:", dashboardOverviewEvaluate);
    }, 2000);
}
// REDIRECT SYSTEM>

// <IMPORT
var automaticImport = function () {
    var modalDialogDisplay = evaluate(page, function () {
        // checking if there was an error.
        if (document.querySelectorAll('[role="dialog"]').length === 0) return 'none';
        return document.querySelectorAll('[role="dialog"]')[0].style.display;
    });

    if (modalDialogDisplay === "none") {
        log(messages.clickingImportButton, 96);
        var importerButton = page.evaluate(function () {
            document.getElementById("btnImport").click();
            return 'Clicked on import button';
        });
        log(messages.clickedImportButton, 96);
    }

    if (modalDialogDisplay !== "none") {
        error.importerError();
    }
}
// IMPORT>

// <SUCCES
var importWasASucces = function () {
    log(messages.importSucces, 32);
    logScreenshot("final.jpg");
    mailHandler.sendMail("Succes", "Succesfully imported the data for: " + processedDate, getScreenshotURL("final.jpg"));
}
// SUCCES>

// <URL HANDLER
var urls = [
    {
        urlContains: "Login",
        action: login.init
    },
    {
        urlContains: "MenuPortal",
        action: moveAwayFromIframeSystem
    },
    {
        urlContains: "financial",
        action: moveToImporterFromDashboard
    },
    {
        urlContains: "CflImport",
        action: automaticImport
    },
    {
        urlContains: "MenuStart",
        action: importWasASucces
    }
]
// URL HANDLER>

// <MAIL HANDLER
var mailHandler = {};
mailHandler.succes = function (requestData) {
    log(messages.emailRequestSuccesful, requestData);
    log("Made request to email server", 32);
    phantom.exit();
}
mailHandler.error = function (requestData) {
    log(messages.emailRequestFailed + "\n" + JSON.stringify(requestData), error.color);
    phantom.exit();
}
mailHandler.sendMail = function (status, message, screenshot) {
    var urlBase = "/mail-handler/";
    var urlStatus = "status=" + status;
    var urlMessage = "&message=" + message;
    var urlScreenshot = "&screenshotURL" + getScreenshotURL(screenshot);
    var url = urlBase + encodeURIComponent(urlStatus + urlMessage + screenshot);
    mailHandler.request(url);
}
mailHandler.request = function (url) {
    logToConsole("Making request to email script with URL: " + url, 37);
    xhrRequest({
        url: url,
        succes: mailHandler.succes,
        error: mailHandler.error
    })
}
// MAIL HANDLER>

// <ERROR HANDLERS
page.onError = function (errorMessage) {
    var message = "PhantomJS encountered an error: " + errorMessage;
    log(message, error.color);
    logScreenshot("error.jpg");
    log("Sourcecode of the website:");
    log("\n" + page.content);
    mailHandler.sendMail("Error: #004: PhantomJS encountered an error, it probably couldn't find an element it needs. ", message, getScreenshotURL("error.jpg"));
}
var error = {
    color: 91
};
error.pageLoadedURLNotFound = function (url) {
    console.log(url);
    var message = messages.URLMissmatch + url;
    logScreenshot("pageNotFound.jpg");
    log(message, error.color);
    mailHandler.sendMail("Error: #001: page requested doesn't exist in logic", message, "pageNotFound.jpg");
};
error.loginFailed = function (loggedIn) {
    logScreenshot("final.jpg");
    log(loginError, error.color);
    log("\n" + loggedIn, error.color);
    mailHandler.sendMail("Error: #002: Login failed", message, "final.jpg");
}
error.pageTryingToLoadURLNotFound = function (url) {
    var message = page.url + messages.pageTryingToLoadURLNotFound;
    log(message, error.color);
    // not exiting Phantom because it's still loading the page.
}
error.moveImporterModalInView = function () {
    var modalDialogText = evaluate(page, function () {
        // checking if there was an error.
        var modalQuery = document.querySelectorAll('[role="dialog"]');
        if (modalQuery.length === 0) {
            return 'none';
        }
        modal = modalQuery[0];
        modal.style.left = 0;
        modal.style.top = 0;
        return modal.innerText;
    });
    return modalDialogText;
}
error.importerError = function () {
    modalDialogText = error.moveImporterModalInView();
    // The system got an error
    logScreenshot("final.png");
    log(messages.importerError, error.color);
    log('\n' + modalDialogText, error.color);
    mailHandler.sendMail("Error: #003: Importer failed", modalDialogText, "final.jpg");

    console.log(messages.doneFlow);
}
// ERROR HANDLERS>

// <PAGE HANDLERS
page.onUrlChanged = function (status) {
    if (page.url === "about:blank") {
        log(messages.logEndMessage);
        logToConsole(messages.phantomEndMessage, 37);
        return false;
    }
    if (page.url !== "about:blank") {
        logToConsole("\n");
        log(messages.URLChanged + page.url, 35);

        var urlMatched = false;
        urls.forEach(function (url) {
            if (page.url.indexOf(url.urlContains) > -1) urlMatched = true;
        });
        if (urlMatched === true) {
            log(page.url + messages.URLExpected, 37);
        } else {
            error.pageTryingToLoadURLNotFound();
        }
    }
}
page.onLoadFinished = function (status) {
    log("Loaded: " + page.url + " status: " + status, 32);
    log(messages.scriptExecuteFlow);
    var executeAfterLoop;
    var urlMatched = false;
    urls.forEach(function (url) {
        var logMessage = "    " + url.urlContains + " " + ((page.url.indexOf(url.urlContains) > -1) ? "<=" : "");
        log(logMessage, 32);
        if (page.url.indexOf(url.urlContains) > -1) {
            executeAfterLoop = url.action;
            urlMatched = true;
        }
    });
    if (urlMatched === false) {
        error.pageLoadedURLNotFound(page.url);
        return false;
    }
    executeAfterLoop();
}
// PAGE HANDLERS>

// <HELPER FUNCTIONS
// Request.
var xhrRequest = function (info) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', info.url);
    xhr.onload = function () {
        if (xhr.status === 200) {
            info.succes(JSON.parse(xhr.responseText));
        } else {
            info.error(JSON.parse(xhr.responseText));
        }
    };
    xhr.onerror = function (errorMessage) {
        info.error(errorMessage);
    }
    xhr.send();
};
/*
 * This function wraps WebPage.evaluate, and offers the possibility to pass
 * parameters into the webpage function. The PhantomJS issue is here:
 * 
 *   http://code.google.com/p/phantomjs/issues/detail?id=132
 * 
 * This is from comment #43.
 */
function evaluate(page, func) {
    var args = [].slice.call(arguments, 2);
    var fn = "function() { return (" + func.toString() + ").apply(this, " + JSON.stringify(args) + ");}";
    return page.evaluate(fn);
}
// HELPER FUNCTIONS>

var init = function () {
    logToConsole(messages.start, 37, 1);
    logInit();
    retrieveAccountInfo();
    log(messages.openRequest + startURL, 35);
    page.open(startURL);
}

init();
